# Equipment Borrowing and Returning System for Academic Department | Strapi v4

ชื่อโครงงาน
    ระบบยืม-คืนอุปกรณ์สาขาวิชา
    Equipment Borrowing and Returning System for Academic Department
จัดทำโดย
    นายสุริยา แสนหาญ (suriya.sa@rmuti.ac.th)
    นายภาณุพงศ์ แซ่ตั้ง (panupong.st@rmuti.ac.th)

### `Prerequisites`
    - node.js : v20
    - npm : v6 and above, or yarn (แนะนำ)
    - postgreSQL : v14 and above

### `install`

โคลนแอปพลิเคชัน

```
git clone https://gitlab.com/suriya.sa/strapi-be.git
```

เข้าโฟลเดอร์แอปพลิเคชัน

```
cd project-name
```

ติดตั้ง

```
yarn install
# or
npm install
```

### `env`

สร้างไฟล์ .env แล้วตั้งค่าไฟล์ .env

```
HOST=0.0.0.0
PORT=1337
APP_KEYS="toBeModified1,toBeModified2"
API_TOKEN_SALT=tobemodified
ADMIN_JWT_SECRET=tobemodified
TRANSFER_TOKEN_SALT=tobemodified

# Database
DATABASE_CLIENT=postgres
DATABASE_HOST=127.0.0.1
DATABASE_PORT=5432
DATABASE_NAME=borrowing-return-app
DATABASE_USERNAME=postgres
DATABASE_PASSWORD=password
DATABASE_SSL=false
```

### `import basic data`

import ข้อมูลพื้นฐาน (ข้อมูลคณะและสาขาวิชา)

```
cd project-name
```
decryption key: rmuti123
```
yarn strapi import -f Basic-information.tar.gz.enc
# or
npm run strapi import -- -f Basic-information.tar.gz.enc
```

### `develop`
Start your Strapi application with autoReload enabled. [Learn more](https://docs.strapi.io/dev-docs/cli#strapi-develop)
เริ่มแอปพลิเคชัน Strapi โดยเปิดใช้งานการโหลดอัตโนมัติ

```
yarn develop
# or
npm run dev
```

### `start`
Start your Strapi application with autoReload disabled. [Learn more](https://docs.strapi.io/dev-docs/cli#strapi-start)
เริ่มแอปพลิเคชัน Strapi โดยปิดใช้งานการโหลดอัตโนมัติ

```
npm run start
# or
yarn start
```

### `build`

Build your admin panel. [Learn more](https://docs.strapi.io/dev-docs/cli#strapi-build)
สร้างแผงผู้ดูแลระบบ

```
npm run build
# or
yarn build
```

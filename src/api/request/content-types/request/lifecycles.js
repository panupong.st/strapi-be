module.exports = {

  async afterUpdate(event) {
    let { result } = event;

    const date = new Date();
    date.setUTCHours(date.getUTCHours() + 7);

    const request = await strapi.db.query("api::request.request").findOne({
      select: ["id"],
      where: { id: result.id },
      populate: { user: true, equipment: true },
    });

    if (result.status === 2) {
      try {
        const notification = await strapi.entityService.create(
          "api::notification.notification",
          {
            data: {
              request: {
                id: request.id,
              },
              title: "อนุมัติยืมเรียบร้อย",
              description: "คำร้องขอยืมของท่านได้รับการอนุมัติเรียบร้อย",
              readed: false,
              publishedAt: date.toISOString(),
            },
          }
        );

        await strapi.entityService.update(
          "plugin::users-permissions.user",
          request.user.id,
          {
            data: {
              notifications: {
                connect: [notification.id],
              },
            },
          }
        );

        await strapi.plugins["email"].services.email.send({
          to: request.user.email,
          from: "ระบบยืม-คืนอุปกรณ์สาขาวิชา <borrowing.and.return.rmuti@gmail.com>",
          replyTo: "borrowing.and.return.rmuti@gmail.com",
          subject: "อนุมัติยืมเรียบร้อย",
          html: `
              <h2>คำร้องขอยืมอุปกรณ์ของท่านได้รับการอนุมัติแล้ว</h2>
              <p>รายการที่ : ${result.id}</p>
              <p>อุปกรณ์ : ${request.equipment.name}</p>
              <p>จาก : ${request.equipment.provider_name}</p>
              <p>กำหนดการคืนอุปกรณ์ : ${result.return_schedule}</p>
              <p>ผู้อนุมัติยืม : ${result.giver}</p>
              `,
        });
      } catch (err) {
        console.log(err);
      }
    }

    if (result.status === 3) {
      try {
        const notification = await strapi.entityService.create(
          "api::notification.notification",
          {
            data: {
              request: {
                id: request.id,
              },
              title: "ครบกำหนดคืนอุปกรณ์",
              description:
                "รายการยืมของท่านครบกำหนดการคืนแล้ว กรุณาคืนอุปกรณ์ภายในวันที่กำหนด",
              readed: false,
              publishedAt: date.toISOString(),
            },
          }
        );

        await strapi.entityService.update(
          "plugin::users-permissions.user",
          request.user.id,
          {
            data: {
              notifications: {
                connect: [notification.id],
              },
            },
          }
        );

        await strapi.plugins["email"].services.email.send({
          to: request.user.email,
          from: "ระบบยืม-คืนอุปกรณ์สาขาวิชา <borrowing.and.return.rmuti@gmail.com>",
          replyTo: "borrowing.and.return.rmuti@gmail.com",
          subject: "ครบกำหนดคืนอุปกรณ์",
          html: `
              <h2>ครบกำหนดการคืนอุปกรณ์</h2>
              <p>กรุณาคืนอุปกรณ์ภายในวันที่กำหนด</p>
              <p>รายการที่ : ${result.id}</p>
              <p>อุปกรณ์ : ${request.equipment.name}</p>
              <p>จาก : ${request.equipment.provider_name}</p>
              <p>กำหนดการคืนอุปกรณ์ : ${result.return_schedule}</p>
              <p>ผู้อนุมัติยืม : ${result.giver}</p>
              `,
        });
      } catch (err) {
        console.log(err);
      }
    }

    if (result.status === 5) {
      try {
        const notification = await strapi.entityService.create(
          "api::notification.notification",
          {
            data: {
              request: {
                id: request.id,
              },
              title: "อนุมัติคืนเรียบร้อย",
              description: "คำร้องขอคืนของท่านได้รับการอนุมัติเรียบร้อย",
              readed: false,
              publishedAt: date.toISOString(),
            },
          }
        );

        await strapi.entityService.update(
          "plugin::users-permissions.user",
          request.user.id,
          {
            data: {
              notifications: {
                connect: [notification.id],
              },
            },
          }
        );

        await strapi.plugins["email"].services.email.send({
          to: request.user.email,
          from: "ระบบยืม-คืนอุปกรณ์สาขาวิชา <borrowing.and.return.rmuti@gmail.com>",
          replyTo: "borrowing.and.return.rmuti@gmail.com",
          subject: "อนุมัติคืนเรียบร้อย",
          html: `
            <h2>คำร้องขอคืนอุปกรณ์ของท่านได้รับการอนุมัติแล้ว</h2>
            <p>รายการที่ : ${event.result.id}</p>
            <p>อุปกรณ์ : ${request.equipment.name}</p>
            <p>จาก : ${request.equipment.provider_name}</p>
            <p>กำหนดการคืนอุปกรณ์ : ${event.result.return_schedule}</p>
            <p>ผู้อนุมัติยืม : ${event.result.giver}</p>
            <p>ผู้อนุมัติคืน : ${event.result.receiver}</p>
            `,
        });
      } catch (err) {
        console.log(err);
      }
    }

    if (result.status === 0) {
      try {
        const notification = await strapi.entityService.create(
          "api::notification.notification",
          {
            data: {
              request: {
                id: request.id,
              },
              title: "คำร้องขอยืมถูกยกเลิก",
              description: "คำร้องขอยืมอุปกรณ์ของท่านถูกยกเลิก",
              readed: false,
              publishedAt: date.toISOString(),
            },
          }
        );

        await strapi.entityService.update(
          "plugin::users-permissions.user",
          request.user.id,
          {
            data: {
              notifications: {
                connect: [notification.id],
              },
            },
          }
        );

        await strapi.plugins["email"].services.email.send({
          to: request.user.email,
          from: "ระบบยืม-คืนอุปกรณ์สาขาวิชา <borrowing.and.return.rmuti@gmail.com>",
          replyTo: "borrowing.and.return.rmuti@gmail.com",
          subject: "รายการยืมถูกยกเลิก",
          html: `
              <h2>คำร้องขอยืมอุปกรณ์ของท่านถูกยกเลิก</h2>
              <p>รายการที่ : ${result.id}</p>
              <p>อุปกรณ์ : ${request.equipment.name}</p>
              <p>จาก : ${request.equipment.provider_name}</p>
              `,
        });
      } catch (err) {
        console.log(err);
      }
    }
  },
};

// Document : https://docs.strapi.io/dev-docs/configurations/cron

module.exports = {
  requestStatusCheck: {
    task: async ({ strapi }) => {
      console.log("Checking.....");

      const options = { timeZone: "Asia/Bangkok" };
      const today = new Date().toLocaleDateString("en-US", options);
      const [month, date, year] = today.split('/');
      const formattedDate = `${year}-${month.padStart(2, '0')}-${date.padStart(2, '0')}`;
      console.log("today : ", formattedDate);

      const requests = await strapi.db.query("api::request.request").findMany({
        where: {
          $and: [
            {
              status: {
                $eq: 2,
              },
            },
            {
              return_schedule: {
                $eq: formattedDate,
              },
            },
          ],
        },
      });

      if (requests.length === 0) {
        console.log("No Request : ", requests);
      } else {
        for (let i = 0; i < requests.length; i++) {
          const request = requests[i];

          const updated = await strapi.db.query("api::request.request").update({
            where: { id: request.id },
            data: {
              status: 3,
            },
          });

          console.log("Request ID : ", updated.id);
        }
      }
    },
    options: {
      rule: "1 0 0 * * *",
      tz: "Asia/Bangkok",
    },
  },
};
